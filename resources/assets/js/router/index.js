import Vue from 'vue';
import VueRouter from 'vue-router';
import AppLayout from '../views/layout/AppLayout';
import Dashboard from '../views/Dashboard';
import Roles from '../views/role/Roles';
import RolesCreate from '../views/role/RolesCreate';
import RolesEdit from '../views/role/RolesEdit';
import Users from '../views/user/Users';
import UserDetail from '../views/user/UserDetail';
import UserCreate from '../views/user/UserCreate';
import UserEdit from '../views/user/UserEdit';
import Admin from '../views/admin/Admin';
import AdminDetail from '../views/admin/AdminDetail';
import AdminCreate from '../views/admin/AdminCreate';
import AdminEdit from '../views/admin/AdminEdit';
import Posts from '../views/post/Posts';
import PostDetail from '../views/post/PostDetail';
import PostCreate from '../views/post/PostCreate';
import PostEdit from '../views/post/PostEdit';

import Categories from '../views/category/Categories';
import CategoryDetail from '../views/category/CategoryDetail';
import CategoryCreate from '../views/category/CategoryCreate';
import CategoryEdit from '../views/category/CategoryEdit';

import Media from '../views/Media';
import Pages from '../views/Pages';
import Tools from '../views/Tools';
import Settings from '../views/Settings';
import Login from '../views/login/login';
import store from '../store';
Vue.use(VueRouter);
const router = new VueRouter({
    routes:[
        {
            path:'/',
            name: 'app-layout',
            component:AppLayout,
            children: [
                {
                    path: '',
                    name: 'dashboard',
                    component: Dashboard,
                    meta: {
                        keepAlive: true
                    }
                },
                {
                    path: '/roles',
                    name: 'roles',
                    component: Roles,
                    meta: {
                        keepAlive: true
                    }
                },
                {
                    path: '/roles/create',
                    name: 'roles_create',
                    component: RolesCreate,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/roles/:id/edit',
                    name: 'roles_edit',
                    component: RolesEdit,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/users',
                    name: 'users',
                    component: Users,
                    meta: {
                        keepAlive: true
                    }
                },
                {
                    path: '/users/create',
                    name: 'user_create',
                    component: UserCreate,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/users/:id',
                    name: 'user_detail',
                    component: UserDetail,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/users/:id/edit',
                    name: 'user_edit',
                    component: UserEdit,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/admin',
                    name: 'admin',
                    component: Admin,
                    meta: {
                        keepAlive: true
                    }
                },
                {
                    path: '/admin/create',
                    name: 'admin_create',
                    component: AdminCreate,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/admin/:id',
                    name: 'admin_detail',
                    component: AdminDetail,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/admin/:id/edit',
                    name: 'admin_edit',
                    component: AdminEdit,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/posts',
                    name: 'posts',
                    component: Posts,
                    meta: {
                        keepAlive: true
                    }
                },
                {
                    path: '/post/create',
                    name: 'post_create',
                    component: PostCreate,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/post/:id',
                    name: 'post_detail',
                    component: PostDetail,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/post/:id/edit',
                    name: 'post_edit',
                    component: PostEdit,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/categories',
                    name: 'categories',
                    component: Categories,
                    meta: {
                        keepAlive: true
                    }
                },
                {
                    path: '/category/create',
                    name: 'category_create',
                    component: CategoryCreate,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/category/:id',
                    name: 'category_detail',
                    component: CategoryDetail,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/category/:id/edit',
                    name: 'category_edit',
                    component: CategoryEdit,
                    meta: {
                        keepAlive: false
                    }
                },
                {
                    path: '/media',
                    name: 'media',
                    component: Media,
                    meta: {
                        keepAlive: true
                    }
                },
                {
                    path: '/pages',
                    name: 'pages',
                    component: Pages,
                    meta: {
                        keepAlive: true
                    }
                },
                {
                    path: '/categories',
                    name: 'categories',
                    component: Categories,
                    meta: {
                        keepAlive: true
                    }
                },
                {
                    path: '/tools',
                    name: 'tools',
                    component: Tools,
                    meta: {
                        keepAlive: true
                    }
                },
                {
                    path: '/setting',
                    name: 'setting',
                    component: Settings,
                    meta: {
                        keepAlive: true
                    }
                },
            ]
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        }
    ]
});
router.beforeEach((to,from,next)=>{
    let routes = store.state.routes;
    if(!from.meta.keepAlive) {
        if (routes.has(from.name)) {
            routes.delete(from.name);
        }
    }
    if (routes.has(to.name)) {
        routes.delete(to.name);
    }
    routes.set(to.name, to.name);
    next();
});

export default router;
