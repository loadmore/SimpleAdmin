const host = 'http://admin.io';
const api = {
    host: host,
    file: host+'/storage',
    api: host+'/api'
};
export default api;