import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user'
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex);
const store = new Vuex.Store({

    plugins: [createPersistedState({
        reducer(val) {
            return {
                // 只储存state中的assessmentData
                user: val.user
            }
        }
    })],
    state: {
        routes: new Map(),
    },
    actions: {

    },
    mutations:{

    },
    modules:{
        user
    }
});
export default store;
