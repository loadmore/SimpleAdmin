
export default  {
    namespaced: true,
    states: {
        token: '',
        user: {}
    },
    actions: {
        Login({commit,state},params) {
            return new Promise((resolve,reject)=>{
                let param = new URLSearchParams();
                param.append('email',params.userName);
                param.append('password',params.password);
                param.append('remember',params.remember);
                http.post('/api/admin/login',param).then((data)=>{
                    if (data.code === 200) {
                        console.log(data)
                        commit('setToken',data.data.access_token);
                        resolve(data.data.access_token)
                    } else {
                        reject(data.message)
                    }
                })
            });
        },

        getUserInfo({commit,state},access_token) {
            return new Promise((resolve,reject)=>{
                let param = new URLSearchParams();
                param.append('token',access_token);
                http.post('/api/admin/info',param).then((data)=>{
                    if (data.code == 200) {
                        commit('setUserInfo',data)
                        resolve()
                    } else {
                        reject(data.message)
                    }
                })
            });
        }
    },
    mutations: {
        setToken(state,token) {
            state.token = token
        },
        setUserInfo(state,user) {
            state.user = user;
        }
    }
};
