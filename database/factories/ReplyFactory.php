<?php

use App\Models\Reply;
use Faker\Generator as Faker;

$factory->define(Reply::class, function (Faker $faker) {
    return [
        'content'=>$faker->text,
        'parent_id'=>0
    ];
});
