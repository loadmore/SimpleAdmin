<?php

use App\Models\Post;
use App\Models\Reply;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReplySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = app(Faker\Generator::class);
        $user_ids = User::all()->pluck('id')->toArray();
        $post_ids = Post::all()->pluck('id')->toArray();

        $replies = factory(Reply::class,1000)->make()->each(function ($reply,$index) use ($user_ids,$post_ids,$faker) {
           // $reply->user_id = $faker->
            $reply->user_id = $faker->randomElement($user_ids);
            $reply->post_id = $faker->randomElement($post_ids);
        });
        Reply::query()->insert($replies->toArray());

    }
}
