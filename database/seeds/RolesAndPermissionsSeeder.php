<?php

use App\Models\Manager;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use App\Services\PermissionUtil;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached role and permissions
        app()['cache']->forget('spatie.permission.cache');
        // create role and assign created permissions

        PermissionUtil::generateFor('users','admin');
        PermissionUtil::generateFor('roles','admin');
        PermissionUtil::generateFor('settings','admin');
        PermissionUtil::generateFor('menus','admin');


        $role_super_admin = Role::create(['guard_name'=>'admin','name' => 'super-admin']);
        $role_admin = Role::create(['guard_name'=>'admin','name' => 'admin']);


        $role_super_admin->givePermissionTo(Permission::all());
        $role_admin->givePermissionTo(Permission::all());


        $super_admin = Manager::query()->firstOrCreate(['name'=>'超级管理员'],['name'=>'超级管理员','email'=>'admin@loadmore.com','password'=>Hash::make('5941wk')]);
        $super_admin->assignRole($role_super_admin);

        $admin = Manager::query()->firstOrCreate(['name'=>'马云'],['name'=>'马云','email'=>'mayun@163.com','password'=>Hash::make('5941wk')]);
        $admin->assignRole($role_admin);

        $admin_2 = Manager::query()->firstOrCreate(['name'=>'雷军'],['name'=>'雷军','email'=>'leijun@163.com','password'=>Hash::make('5941wk')]);
        $admin_2->assignRole($role_admin);




    }
}
