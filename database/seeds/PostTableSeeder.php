<?php

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;
class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = app(Faker\Generator::class);
        $categories_ids = Category::all()->pluck('id')->toArray();
        $user_ids = User::all()->pluck('id')->toArray();
        $posts = factory(Post::class,50)->make()->each(function ($post,$index) use ($categories_ids,$user_ids,$faker){
           $post->author_id = $faker->randomElement($user_ids);
           $post->category_id = $faker->randomElement($categories_ids);
        });
        Post::query()->insert($posts->toArray());
    }
}
