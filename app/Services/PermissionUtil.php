<?php
/**
 * User: Administrator
 * Date: 2018/9/12 0012
 * Time: 11:11
 */

namespace App\Services;


use Spatie\Permission\Models\Permission;

class PermissionUtil
{
    public static function generateFor($name,$guard_name) {
        Permission::create(['guard_name'=>$guard_name,'name'=>'browse_'.$name,'table_name'=>$name]);
        Permission::create(['guard_name'=>$guard_name,'name'=>'read_'.$name,'table_name'=>$name]);
        Permission::create(['guard_name'=>$guard_name,'name'=>'edit_'.$name,'table_name'=>$name]);
        Permission::create(['guard_name'=>$guard_name,'name'=>'add_'.$name,'table_name'=>$name]);
        Permission::create(['guard_name'=>$guard_name,'name'=>'delete_'.$name,'table_name'=>$name]);
    }

    public static function generate($name) {
        Permission::create(['name'=>$name]);
    }

}
