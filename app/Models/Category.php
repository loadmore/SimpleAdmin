<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['parent_id','name','parent_id','order_id','slug'];

    public function childCategory() {
        return $this->hasMany('App\Models\Category','parent_id','id');
    }
    public function children() {
        return $this->childCategory()->with('children');
    }
}
