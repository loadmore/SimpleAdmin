<?php
/**
 * Created by PhpStorm.
 * User: leileisun
 * Date: 2018/10/14
 * Time: 10:57 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ReplyLike extends Model
{
    protected $fillable = ['reply_id','user_id'];


}