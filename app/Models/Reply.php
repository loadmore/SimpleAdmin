<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $fillable = ['user_id','content','parent_id','post_id','like_count'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function like() {
        return $this->hasMany(ReplyLike::class,'reply_id','id');
    }
}
