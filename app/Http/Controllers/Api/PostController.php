<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->get('per_page',10);
        $current_page = $request->get('current_page',1);
        $withPage = $request->get('with_page',false);
        $query = Post::query()->with('category','user')->withCount('replies');
        if ($withPage) {
            $data =  $query->forPage($current_page)->paginate($per_page);
        } else {
            $data = $query->get();
        }
        return $this->success($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request->get('title');
        $author_id = $request->get('author_id');
        $category_id = $request->get('category_id');
        $seo_title = $request->get('seo_title');
        $image = $request->get('image');
        $description = $request->get('description');
        $slug = $request->get('slug');
        $keywords = $request->get('keywords');
        $body = $request->get('body');
        $attrs = [
            'title'=>$title,
            'author_id'=>$author_id,
            'category_id'=>$category_id,
            'seo_title'=>$seo_title,
            'image'=>$image,
            'description'=> $description,
            'slug'=>$slug,
            'body'=>$body,
            'keywords'=>$keywords];
        $post = Post::query()->create($attrs);
        return $post;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::query()->find($id);
        return $this->success($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $title = $request->get('title');
        $author_id = $request->get('author_id');
        $category_id = $request->get('category_id');
        $seo_title = $request->get('seo_title');
        $image = $request->get('image');
        $description = $request->get('description');
        $slug = $request->get('slug');
        $keywords = $request->get('keywords');
        $body = $request->get('body');
        $attrs = [
            'title'=>$title,
            'author_id'=>$author_id,
            'category_id'=>$category_id,
            'seo_title'=>$seo_title,
            'image'=>$image,
            'description'=> $description,
            'slug'=>$slug,
            'body'=>$body,
            'keywords'=>$keywords];
        $post = Post::query()->find($id);
        $post->update($attrs);
        return $post;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
