<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Like;
use Illuminate\Http\Request;

class LikeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attrs = [];
        $user_id = auth('api')->id();
        $post_id = $request->get("post_id");
        $like = Like::query()->where('post_id',$post_id)->where('user_id',$user_id);
        if ($user_id && $post_id && $like) {
            $attrs["user_id"] = $user_id;
            $attrs["post_id"] = $post_id;
            $newLike = Like::query()->create($attrs);
            return $this->success($newLike);
        }else {
            return $this->fail(-1);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post_id = request()->get('post_id');
        $user_id = auth('api')->id();
        $like = Like::query()->where('post_id',$post_id)->where('user_id',$user_id);
        if ($like) {
            $like->delete();
            return $this->success();
        }
        return $this->fail(-1,[]);
    }
}
