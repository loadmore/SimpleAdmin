<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function success($data = [])
    {
        return response()->json([
            'status' => 1,
            'code' => 200,
            'message' => config('error_code.code') [200],
            'data' => $data
        ]);
    }
    public function fail($code,$data = [])
    {
        return response()->json([
            'status' => 0,
            'code' => $code,
            'message' => config('error_code.code') [$code],
            'data' => $data
        ]);
    }
}
