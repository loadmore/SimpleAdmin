<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//$api->middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


$api = app('Dingo\Api\Routing\Router');
$api->version('v1',['namespace'=>'App\Http\Controllers\Api'], function ($api) {

    $api->resource('/user','UserController');
    $api->resource('/admin','ManagerController');
    $api->resource('/role','RoleController');
    $api->resource('/permission','PermissionController');
    $api->resource('/file','FileController');
    $api->resource('/resource','ResourceController');
    $api->resource('/post','PostController');
    $api->resource('/category','CategoryController');
    $api->resource('/reply','ReplyController');
    $api->resource('/like','LikeController');
    $api->group([
        'middleware' => 'api'
    ], function ($api) {
        $api->post('login', 'AuthController@login');
        $api->post('logout', 'AuthController@logout');
        $api->post('refresh', 'AuthController@refresh');
        $api->post('info', 'AuthController@me');

    });
    $api->group([
        'middleware' => 'api',
        'prefix' => 'admin'
    ], function ($api) {
        $api->post('login', 'Admin\AuthController@login');
        $api->post('logout', 'Admin\AuthController@logout');
        $api->post('refresh', 'Admin\AuthController@refresh');
        $api->post('info', 'Admin\AuthController@me');

    });
});
